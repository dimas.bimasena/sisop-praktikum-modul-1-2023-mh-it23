#!/bin/bash

# Untuk membuat log dan folder users jika belum ada
if [ ! -f log.txt ]; then
	touch log.txt
fi

if [ ! -d  ./users ]; then
	mkdir users
fi

if [ ! -f ./users/users.txt ]; then
	touch ./users/users.txt 
fi


# Mendapatkan hari bulan tanggal waktu
datetime=$(date "+%y/%m/%d %H:%M:%S")

# Input username
read -p "Username: " username

# Validasi username
while grep -q "^$username:" ./users/users.txt; do
	echo "$datetime REGISTER: ERROR User already exists" >> log.txt
  echo "Username already exists"
	exit
done

# Input password
read -s -p "Password: " password

# Validasi password
while true; do 
  if [[ ${#password} -lt 8 ]]; then
    read -s -p "Password is too short. Enter a password (minimum 8 characters): " password
    echo ""
  elif ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]] || ! [[ "$password" =~ [0-9] ]]; then
    read -s -p "Password must be alphanumeric with at least one uppercase and one lowercase letter. Enter a password: " password
    echo ""
  elif [[ "$password" == *"$username"* ]]; then
    read -s -p "Password cannot contain username. Enter a password: " password
    echo ""
  elif [[ "$password" == *"chicken"* || "$password" == *"ernie"* ]]; then
    read -s -p "Password cannot contain 'chicken' or 'ernie'. Enter a password: " password
    echo ""
  else
    # Menambahkan data user ke dalam file /users/users.txt
    echo "$username:$password" >> ./users/users.txt
    echo "$datetime REGISTER: INFO User $username registered successfully" >> log.txt
    echo "User '$username' registered successfully."
    break
  fi
done
