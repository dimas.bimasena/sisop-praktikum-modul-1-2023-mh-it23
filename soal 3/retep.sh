#!/bin/bash

# Membuat file log.txt jika belum ada
touch log.txt

# Meminta input username dari pengguna
read -p "Masukkan username: " username

# Mencari informasi username dan password dari file /users/users.txt
user_info=$(grep "^$username:" ./users/users.txt)

# Memeriksa apakah username ada di dalam file /users/users.txt
if [ -z "$user_info" ]; then
    echo "Username tidak terdaftar."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

# Memeriksa apakah password cocok dengan yang ada di dalam file /users/users.txt
read -s -p "Masukkan password: " password
if [ "$password" != "$(echo $user_info | cut -d':' -f2)" ]; then
    echo "Password salah."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

# Jika username dan password cocok, maka tampilkan pesan sukses dan catat ke dalam file log.txt
echo "Login berhasil."
echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
