# sisop-praktikum-modul-1-2023-MH-IT23


# Laporan Resmi Modul 1 Sistem Operasi
Adimasdefatra Bimsasena 5027211040

# Soal 1

Skrip terlihat bagus dan harus berfungsi sebagaimana mestinya. Berikut penjelasan singkat tentang apa yang dilakukan skrip:

Skrip membaca konten file '2023 QS World University Rankings.csv' ke dalam variabel 'bocchi_survey'.

Poin 1: Skrip memfilter data 'bocchi_survey' untuk menyertakan hanya universitas yang berlokasi di Jepang, mengurutkannya berdasarkan peringkat keseluruhannya, dan mencetak 5 universitas teratas dengan nama dan peringkatnya.

Poin 2: Skrip mengurutkan 5 universitas Jepang teratas berdasarkan skor FSR mereka dan mencetak nama universitas dan skor dengan skor FSR terendah.

Poin 3: Skrip mengurutkan universitas Jepang berdasarkan peringkat GER mereka dan mencetak 10 universitas teratas dengan nama dan peringkat GER mereka.

Butir 4: Skrip memfilter data 'bocchi_survey' untuk memasukkan hanya universitas dengan kata 'keren' di namanya dan mencetak nama universitasnya.

Secara keseluruhan, skrip ditulis dengan baik dan harus memberikan hasil yang diharapkan.


```bash
  #!/bin/bash

bocchi_survey=$(cat '2023 QS World University Rankings.csv')

# poin 1
univ_best=$(echo "$bocchi_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -k1 | head -n 5)
echo "Top 5 Universitas di Jepang:"
echo "$univ_best"  | awk -F, '{ print $1 "\t" $2 }'
echo " "

# poin 2
fsr_terendah=$(echo "$univ_best" | sort -n -k9 | tail -n 1)
echo "Universitas dengan FSR Score terendah di antara 5 Universitas teratas:"
echo "$fsr_terendah" | awk -F, '{ print $2 "\t" $9 }'
echo " "

# poin 3
ger_best=$(echo "$bocchi_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -k20 | tail -n 10)
echo "Top 10 Universitas di Jepang dengan GER Rank tertinggi:"
echo "$ger_best" | awk -F, '{ print $1 "\t" $2 "\t" $20 }'
echo " "

# poin 4
univ_keren=$(echo "$bocchi_survey" | grep -i "keren")
echo "Universitas paling keren di dunia:"
echo "$univ_keren" | awk -F, '{ print $1 "\t" $2 }'
```

# Soal 2

Skrip ini tampaknya merupakan skrip untuk mengotomatiskan pengunduhan dan pengarsipan gambar dari internet.

Begini cara kerjanya:

Skrip pertama-tama memeriksa apakah ada direktori bernama "kumpulan_1". Jika tidak, itu membuat direktori.
Itu kemudian mendapatkan jam saat ini menggunakan perintah "tanggal".

Selanjutnya, menghitung jumlah direktori di direktori saat ini yang namanya dimulai dengan "kumpulan_" dan menugaskan nomor tersebut ke variabel "folder".

Jika jam saat ini adalah kelipatan 10, itu membuat direktori baru bernama "kumpulan_N" di mana N adalah nilai "folder" saat ini yang ditambah 1.

Kemudian menghitung jumlah file dalam direktori "kumpulan_N" saat ini yang namanya dimulai dengan "perjalanan_" dan menetapkan nomor tersebut ke variabel "nomor".

Untuk setiap jam dalam sehari hingga jam saat ini, skrip mengunduh gambar dari URL "https://source.unsplash.com/1600x900/?indonesia" dan menyimpannya ke file bernama "kumpulan_N/perjalanan_X.jpg" di mana N adalah nilai "folder" saat ini dan X adalah nilai "nomor" saat ini yang ditambah 1.

Terakhir, skrip memeriksa apakah ada direktori bernama "devil_1". Jika tidak, itu membuat direktori.

Itu kemudian menghitung jumlah file di semua direktori yang namanya dimulai dengan "devil_" dan menetapkan nomor itu ke variabel "zip".

Jika ada file di direktori "devil_" mana pun yang berumur lebih dari 1 hari, skrip akan membuat file ZIP baru bernama "devil_N.zip" di mana N adalah nilai "zip" saat ini ditambah 1, dan mengompres konten direktori "kumpulan_M" di mana M adalah nilai "zip" saat ini. File ZIP yang dihasilkan disimpan ke direktori saat ini.

```bash
#!/bin/bash

# Membuat kumpulan folder yang pertama jika belum ada
if [ ! -d "kumpulan_1" ]; then
mkdir kumpulan_1
fi

# Mendapatkan jam sekarang
jam=$(date +%H)

# Mendapatkan nomor folder kumpulan sekarang
folder=$(ls -d kumpulan_* | wc -l)

# Jika sudah 10 jam sejak folder terakhir dibuat, buat folder terbaru
if [ $((jam%10)) -eq 0 ]; then
folder=$((folder+1))
mkdir "kumpulan_$folder"
fi

# Mendapatkan nomor file gambar yang terakhir
nomor=$(ls kumpulan_$folder/perjalanan_* 2>/dev/null | wc -l)

# Mendownload gambar sebanyak X kali dengan X adalah jam saat ini
for (( i=1; i<=$jam; i++ )); do
nomor=$((nomor+1))
wget -O "kumpulan_$folder/perjalanan_$nomor.jpg" "https://source.unsplash.com/1600x900/?indonesia"
done

## Membuat devil folder  pertama jika belum ada
if [ ! -d "devil_1" ]; then
mkdir devil_1
fi

# Mendapatkan nomor ZIP sekarang
zip=$(ls devil_* | wc -l)

# Jika sudah satu hari sejak ZIP terakhir dibuat, buat ZIP terbaru
if [ $(find devil_*/ -mtime +1 | wc -l) -gt 0 ]; then
zip=$((zip+1))
zip -r "devil_$zip.zip" "kumpulan_$((zip-1))"
fi
done
```
# Soal 3

(louis.sh)

Skrip tersebut tampaknya merupakan skrip pendaftaran pengguna yang meminta nama pengguna dan kata sandi, memvalidasinya, dan menulis data pengguna yang valid ke file ./users/users.txt. Itu juga mencatat upaya pendaftaran yang berhasil dan gagal dalam file log.txt.

Inilah cara kerja skrip:

Ia memeriksa apakah file log log.txt ada. Jika tidak, itu membuat file kosong.
Ia memeriksa apakah direktori ./users ada. Jika tidak, itu membuat direktori.
Ia memeriksa apakah file ./users/users.txt ada. Jika tidak, itu membuat file.
Itu meminta pengguna untuk memasukkan nama pengguna.
Ini memeriksa apakah nama pengguna sudah ada di file ./users/users.txt. Jika ya, error akan dicatat di file log.txt dan keluar dari skrip.
Itu meminta pengguna untuk memasukkan kata sandi.
Ini memvalidasi kata sandi dengan memeriksa bahwa panjangnya minimal 8 karakter, berisi setidaknya satu huruf besar dan satu huruf kecil, dan setidaknya satu angka. Itu juga memeriksa bahwa kata sandi tidak mengandung nama pengguna, dan tidak mengandung kata "ayam" atau "ernie". Jika kata sandi valid, ia menulis nama pengguna dan kata sandi ke file ./users/users.txt dan mencatat upaya pendaftaran yang berhasil di file log.txt. Jika kata sandi tidak valid, itu meminta pengguna untuk memasukkan kata sandi baru.
```bash
#!/bin/bash

# Untuk membuat log dan folder users jika belum ada
if [ ! -f log.txt ]; then
	touch log.txt
fi

if [ ! -d  ./users ]; then
	mkdir users
fi

if [ ! -f ./users/users.txt ]; then
	touch ./users/users.txt 
fi


# Mendapatkan hari bulan tanggal waktu
datetime=$(date "+%y/%m/%d %H:%M:%S")

# Input username
read -p "Username: " username

# Validasi username
while grep -q "^$username:" ./users/users.txt; do
	echo "$datetime REGISTER: ERROR User already exists" >> log.txt
  echo "Username already exists"
	exit
done

# Input password
read -s -p "Password: " password

# Validasi password
while true; do 
  if [[ ${#password} -lt 8 ]]; then
    read -s -p "Password is too short. Enter a password (minimum 8 characters): " password
    echo ""
  elif ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]] || ! [[ "$password" =~ [0-9] ]]; then
    read -s -p "Password must be alphanumeric with at least one uppercase and one lowercase letter. Enter a password: " password
    echo ""
  elif [[ "$password" == *"$username"* ]]; then
    read -s -p "Password cannot contain username. Enter a password: " password
    echo ""
  elif [[ "$password" == *"chicken"* || "$password" == *"ernie"* ]]; then
    read -s -p "Password cannot contain 'chicken' or 'ernie'. Enter a password: " password
    echo ""
  else
    # Menambahkan data user ke dalam file /users/users.txt
    echo "$username:$password" >> ./users/users.txt
    echo "$datetime REGISTER: INFO User $username registered successfully" >> log.txt
    echo "User '$username' registered successfully."
    break
  fi
done
```

(retep.sh)

Script terlihat bagus dan fungsional. Ini pertama-tama memeriksa apakah file log ada dan membuatnya jika tidak. Kemudian, meminta pengguna untuk memasukkan nama pengguna mereka dan mencari informasi pengguna di file users.txt. Jika pengguna tidak ditemukan, skrip keluar dengan pesan kesalahan dan mencatat upaya tersebut di file log. Jika pengguna ditemukan, skrip meminta pengguna untuk memasukkan kata sandinya dan memeriksa apakah kata sandinya benar. Jika kata sandi benar, skrip menampilkan pesan sukses dan mencatat upaya login di file log.

```bash
#!/bin/bash

# Membuat file log.txt jika belum ada
touch log.txt

# Meminta input username dari pengguna
read -p "Masukkan username: " username

# Mencari informasi username dan password dari file /users/users.txt
user_info=$(grep "^$username:" ./users/users.txt)

# Memeriksa apakah username ada di dalam file /users/users.txt
if [ -z "$user_info" ]; then
    echo "Username tidak terdaftar."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

# Memeriksa apakah password cocok dengan yang ada di dalam file /users/users.txt
read -s -p "Masukkan password: " password
if [ "$password" != "$(echo $user_info | cut -d':' -f2)" ]; then
    echo "Password salah."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi
# Jika username dan password cocok, maka tampilkan pesan sukses dan catat ke dalam file log.txt
echo "Login berhasil."
echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
```






# Soal 4

Ini adalah skrip Bash yang melakukan pencadangan file syslog dan mengenkripsi isinya menggunakan sandi shift. Skrip pertama-tama mendapatkan waktu saat ini, dan membuat file cadangan dengan nama dalam format "HH:MM_dd:mm:yyyy.txt". Itu kemudian membuat direktori cadangan jika belum ada, dan menyalin file syslog ke direktori cadangan.

Skrip kemudian menentukan nilai shift dengan menggunakan jam saat ini, membuat alfabet baru yang telah digeser oleh nilai shift yang ditentukan, dan menggunakan perintah sed untuk mengenkripsi konten file cadangan menggunakan alfabet yang baru dibuat. Terakhir, skrip mencetak pesan sukses.

Skrip juga menyertakan kode yang dikomentari yang dapat digunakan sebagai cronjob untuk mencadangkan file syslog secara otomatis setiap 2 jam. Namun, kode ini saat ini tidak digunakan.

Berikut cara kerja skrip:

Skrip mendapatkan waktu saat ini menggunakan perintah tanggal, dan memformatnya menjadi string dalam format "HH:MM dd:mm:yyyy".

Skrip membuat nama file cadangan menggunakan perintah tanggal dan memformatnya menjadi string dalam format "HH:MM_dd:mm:yyyy.txt".

Skrip membuat direktori cadangan menggunakan perintah mkdir jika belum ada. Opsi -d memeriksa apakah direktori yang ditentukan ada, dan ! operator meniadakan hasilnya. Jika direktori yang ditentukan tidak ada, skrip membuatnya menggunakan perintah mkdir.

Skrip menyalin file syslog ke direktori cadangan menggunakan perintah cp. File sumber terletak di direktori /var/log, dan file tujuan terletak di direktori cadangan dengan nama yang dibuat pada langkah 2.

Skrip menentukan nilai pergeseran dengan menggunakan perintah tanggal dan memformatnya menjadi string dalam format "HH". Nilai ini digunakan untuk menggeser alfabet pada langkah berikutnya.

Skrip membuat alfabet baru yang telah digeser oleh nilai pergeseran yang ditentukan menggunakan perintah tr. Argumen pertama menentukan alfabet asli, dan argumen kedua menentukan alfabet yang baru digeser.

Skrip menggunakan perintah sed untuk mengenkripsi konten file cadangan menggunakan alfabet yang baru dibuat. Perintah y di sed digunakan untuk melakukan terjemahan berdasarkan karakter, dan argumen pertama menentukan karakter yang akan diganti, dan argumen kedua menentukan karakter pengganti.

Skrip mencetak pesan sukses.

Secara keseluruhan, skrip membuat cadangan file syslog, mengenkripsi isinya menggunakan sandi shift, dan mencetak pesan sukses. Kode yang dikomentari dapat digunakan sebagai cronjob untuk mencadangkan file syslog secara otomatis setiap 2 jam.
(encrypt)
```bash
#!/bin/bash

# Mendapatkan waktu saat ini
now=$(date +"%H:%M %d:%m:%Y")

# Membuat nama file backup dengan format jam:menit tanggal:bulan:tahun
backup_file_name=$(date +"%H:%M_%d:%m:%Y").txt

# Membuat direktori backup (Jika belum ada)
if [ ! -d "backup" ]; then
  mkdir backup
fi

# Memindahkan file syslog ke direktori backup
sudo cp /var/log/syslog backup/$backup_file_name

# Menentukan shift cipher
shift=$(date +"%H")

# Membuat string manipulation
alphabet="abcdefghijklmnopqrstuvwxyz"
ciphered_alphabet=$(echo $alphabet | tr "${alphabet:0:26}" "${alphabet:${shift}:26}${alphabet:0:${shift}}")
ciphered_alphabet_upper=$(echo $ciphered_alphabet | tr '[:lower:]' '[:upper:]')

# Mengenkripsi isi file log dengan shift cipher yang telah ditentukan
sed -i "y/$alphabet$ciphered_alphabet_upper/$ciphered_alphabet$ciphered_alphabet_upper/" backup/$backup_file_name

echo "File $backup_file_name telah berhasil dienkripsi"

# Pakai cronjobs buat backup file setiap 2 jam
# * 2 * * * sh /home/yuki/Documents/soal4/log_encrypt.sh

# Backup file syslog setiap 2 jam
#if [ $(date +"%H") -eq "00" ] || [ $(date +"%H") -eq "02" ] || [ $(date +"%H") -eq "04" ] || [ $(date +"%H") -eq "06" ] || [ $(date +"%H") -eq "08" ] || [ $(date +"%H") -eq "10" ] || [ $(date +"%H") -eq "12" ] || [ $(date +"%H") -eq "14" ] || [ $(date +"%H") -eq "16" ] || [ $(date +"%H") -eq "18" ] || [ $(date +"%H") -eq "20" ] || [ $(date +"%H") -eq "22" ]
#then
#  cp ./backup/$(date +"%H:%M_%d:%m:%Y").txt
#fi
```
(encrypt)


Ini adalah skrip Bash untuk mendekripsi file menggunakan sandi shift. Skrip meminta pengguna untuk memasukkan nama file yang akan didekripsi, dan kemudian menentukan sandi shift dengan mengekstraksi nilai jam dari nama file. Ini kemudian melakukan manipulasi string untuk membuat alfabet baru yang telah digeser oleh nilai pergeseran yang ditentukan. Terakhir, skrip menggunakan perintah sed untuk mengganti karakter dalam file dengan rekan yang didekripsi berdasarkan alfabet yang baru dibuat.

Berikut cara kerja skrip:

Pengguna diminta untuk memasukkan nama file yang akan didekripsi dalam format "HH:MM_dd:mm:yyyy.txt".

Skrip mengekstrak nilai jam dari nama file dengan menggunakan perintah cut dua kali. Perintah cut pertama mengekstrak bagian pertama dari nama file sebelum garis bawah, dan perintah cut kedua mengekstrak nilai jam sebelum titik dua pertama.

Skrip membuat alfabet baru yang telah digeser nilai pergeseran yang ditentukan dengan menggunakan perintah tr. Perintah tr membutuhkan dua argumen: argumen pertama menentukan karakter yang akan diganti, dan argumen kedua menentukan karakter pengganti. Dalam hal ini, argumen pertama adalah alfabet asli dan versi huruf besarnya, dan argumen kedua adalah alfabet yang baru digeser dan versi huruf besarnya.

Skrip menggunakan perintah sed untuk mengganti karakter dalam file dengan rekan yang didekripsi berdasarkan alfabet yang baru dibuat. Perintah sed memiliki tiga argumen: argumen pertama menentukan karakter yang akan diganti, argumen kedua menentukan karakter pengganti, dan argumen ketiga menentukan file yang akan dimodifikasi. Dalam hal ini, argumen pertama adalah kombinasi dari alfabet asli dan versi huruf besarnya, dan argumen kedua adalah kombinasi dari alfabet yang baru digeser dan versi huruf besarnya.

Skrip mencetak pesan sukses setelah proses dekripsi selesai.

Secara keseluruhan, skrip menggunakan perintah Bash dasar seperti echo, read, cut, tr, dan sed untuk mendekripsi file menggunakan shift cipher.
(decrypt)

```bash
#!/bin/bash

# Mendapatkan nama file yang akan didekripsi
echo "Masukkan nama file yang akan didekripsi (format: HH:MM_dd:mm:yyyy.txt):"
read file_name

# Menentukan shift cipher dari waktu pada nama file
shift=$(echo $file_name | cut -d'_' -f1 | cut -d':' -f1)

# Membuat string manipulation dengan shift cipher yang ditentukan
alphabet="abcdefghijklmnopqrstuvwxyz"
ciphered_alphabet=$(echo $alphabet | tr "${alphabet:${shift}:26}${alphabet:0:${shift}}" "${alphabet:0:26}")
ciphered_alphabet_upper=$(echo $ciphered_alphabet | tr '[:lower:]' '[:upper:]')

# Mendekripsi isi file dengan shift cipher yang telah ditentukan
sudo sed -i "y/$alphabet$ciphered_alphabet_upper/$ciphered_alphabet$ciphered_alphabet_upper/" $file_name

echo "File $file_name telah berhasil didekripsi."
```
(decrypt)


